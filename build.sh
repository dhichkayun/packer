#!/bin/bash

# Define packer build commands before pushing into repository
# Example: packer build -only='step1.docker.example' .
packer build -var "boundary_ami_version=0.7.4" -var "AWS_ACCESS_KEY=$AWS_ACCESS_KEY_ID" -var "AWS_SECRET_KEY=$AWS_SECRET_ACCESS_KEY" ./boundary/boundary.pkr.hcl