#!/bin/bash

# Define packer inpsect commands before merging into main
# Example: packer inspect template.pkr.hcl

packer inspect -var "boundary_ami_version=0.7.4" -var "AWS_ACCESS_KEY=$AWS_ACCESS_KEY_ID" -var "AWS_SECRET_KEY=$AWS_SECRET_ACCESS_KEY" ./boundary/boundary.pkr.hcl
