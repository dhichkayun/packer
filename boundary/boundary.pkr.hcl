variable "boundary_ami_version" {
  type    = string
}
variable "AWS_ACCESS_KEY" {
  type    = string
  sensitive = true
}
variable "AWS_SECRET_KEY" {
  type    = string
  sensitive = true
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }
source "amazon-ebs" "boundary-controller" {
  access_key    = "${var.AWS_ACCESS_KEY}"
  ami_name      = "boundary-${var.boundary_ami_version}-controller"
  instance_type = "t2.nano"
  region        = "ap-south-1"
  secret_key    = "${var.AWS_SECRET_KEY}"
  source_ami_filter {
    filters = {
      name = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
      root-device-type = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

build {
  sources = ["sources.amazon-ebs.boundary-controller"]
  provisioner "file" {
    source = "./boundary/install-script.sh"
    destination = "/tmp/install-script.sh"
  }
  provisioner "shell"{
    inline = ["sudo /tmp/install-script.sh controller"]
  }

}
